package P8_9;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 1:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class CombLockDriver {
    public static void main(String[] args) {
        System.out.print("Please enter the tree clicks, enter \"Q\" to exit: ");
        Scanner sc = new Scanner(System.in);
        int nClicks1 = sc.nextInt();
        int nClicks2 = sc.nextInt();
        int nClicks3 = sc.nextInt();
        CombLock mCombLock = new CombLock(10, 20, 5);
        mCombLock.open(nClicks1, nClicks2, nClicks3);
        String LockStatus = mCombLock.isOpen() ? "Opened" : "Locked";
        System.out.println("The Lock's status is: " + LockStatus);
    }
}
