package P8_9;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 12:51 PM
 * P8.9 Declare a class that works like the combination lock.
 */
public class CombLock {
    private int secret1,secret2,secret3;
    private int nDial;
    private boolean openStatus;
    private int count;

    public CombLock(int secret1, int secret2, int secret3) {
        this.secret1=secret1;
        this.secret2=secret2;
        this.secret3=secret3;
        count = 0;
        openStatus= false;
        reset();
    }

    public void reset() {
        nDial=0;
    }
    public void turnRight(int ticks) {
        count++;
        if (count == 1) {
            reset();
            nDial +=ticks;
        }
        else if (count == 3) {
            if (secret3 > secret2) nDial += ticks;
            else nDial = ticks + secret2 - 40;
        }
    }
    public void turnLeft(int ticks) {
        nDial = (80 - ticks + nDial);
        count++;
    }
    public void open(int ticks1, int ticks2, int ticks3) {
        reset();
        turnRight(ticks1);
        if (nDial == secret1) {
            turnLeft(ticks2);
            if (nDial == secret2) {
                turnRight(ticks3);
                if (nDial == secret3) openStatus=true;
            }
        }
    }
    public boolean isOpen() {
        return this.openStatus;
    }

}
