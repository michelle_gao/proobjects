package P8_14;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 2:34 PM
 * P8_14 Write static methods
 */
public class Geometry {
    public static double sphereVolume(double r) {
        return 4 / 3 * Math.PI * Math.pow(r, 3);
    }
    public static double sphereSurface(double r) {
        return 4 * Math.PI * r * r;
    }

    public static double cylinderVolume(double r, double h) {
        return Math.PI * r * r * h;
    }

    public static double cylinderSurface(double r, double h) {
        return 2*Math.PI * r * r + 2 * Math.PI * r * h;
    }

    public static double coneVolume(double r, double h) {
        return Math.PI * r * r * h /3;
    }

    public static double coneSurface(double r, double h) {
        return  Math.PI * r * r + Math.PI * r * h;
    }
}
