package P8_14;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class GeometryDriver {
    public static void main(String[] args) {
        System.out.print("Please enter a r value and h value and \"Q\" to exit:)");
        Scanner sc = new Scanner(System.in);
        double r = sc.nextDouble();
        double h = sc.nextDouble();
        System.out.println(Geometry.sphereVolume(r));
        System.out.println(Geometry.sphereSurface(r));
        System.out.println(Geometry.cylinderVolume(r,h));
        System.out.println(Geometry.cylinderSurface(r, h));
        System.out.println(Geometry.coneVolume(r,h));
        System.out.println(Geometry.coneSurface(r,h));
    }
}
