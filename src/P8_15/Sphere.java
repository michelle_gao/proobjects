package P8_15;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sphere {
    private double r;
    public Sphere(double r) {
        this.r = r;
    }

    public double sphereVolume() {
        return 4 / 3 * Math.PI * Math.pow(r, 3);
    }
    public double sphereSurface() {
        return 4 * Math.PI * r * r;
    }

}
