package P8_15;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class Cone extends Cylinder {

    public Cone() {}

    public double coneVolume() {
        return (cylinderVolume())/3;
    }
    public double coneSurface() {
        return (cylinderSurface())/2;
    }
}

