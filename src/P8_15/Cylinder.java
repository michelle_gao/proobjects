package P8_15;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class Cylinder {
    private double r;
    private double h;
    public Cylinder() {}
    public void setR(double r) {
        this.r =r;
    }

    public void setH(double h) {
        this.h=h;
    }

    public double cylinderVolume() {
        return Math.PI * r * r * h;
    }
    public double cylinderSurface() {
        return 2*Math.PI * r * r + 2 * Math.PI * r * h;
    }
}
