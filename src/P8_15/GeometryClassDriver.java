package P8_15;

import java.util.MissingFormatWidthException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 3:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class GeometryClassDriver {
    public static void main(String[] args) throws MissingFormatWidthException{
        System.out.print("please enter the value r and h and \"Q\" to exit:");
        Scanner sc = new Scanner(System.in);
        double r = sc.nextDouble();
        double h = sc.nextDouble();
        Sphere mSphere = new Sphere(r);
        System.out.println(mSphere.sphereVolume());
        System.out.println(mSphere.sphereSurface());
        Cylinder mCylinder = new Cylinder();
        mCylinder.setR(r);
        mCylinder.setH(h);
        System.out.println(mCylinder.cylinderVolume());
        System.out.println(mCylinder.cylinderSurface());
        Cone mCone = new Cone();
        mCone.setR(r);
        mCone.setH(h);
        System.out.println(mCone.coneVolume());
        System.out.println(mCone.coneSurface());
    }
}
