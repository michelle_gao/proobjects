package P8_4;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/18/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class Address {
    private String strHouseNumber;
    private String strStreet;
    private String strAptNumber;
    private String strCity;
    private String strState;
    private int nZipCode;
    public Address(String strHouseNumber,String strStreet,String strAptNumber, String strCity,String strState,
                   int nZipCode)  {
        this.strHouseNumber = strHouseNumber;
        this.strStreet = strStreet;
        this.strState = strState;
        this.strAptNumber = strAptNumber;
        this.strCity = strCity;
        this.nZipCode = nZipCode;
    }
    public Address(String strHouseNumber,String strStreet,String strCity,String strState,
                   int nZipCode)  {
        this.strHouseNumber = strHouseNumber;
        this.strStreet = strStreet;
        this.strState = strState;
        this.strCity = strCity;
        this.nZipCode = nZipCode;
    }
    public int getZipCode() {
        return this.nZipCode;
    }
    public void print() {
        System.out.println(strHouseNumber + " " + strStreet + " " + strAptNumber);
        System.out.println(strCity + " " + strState + " " + nZipCode);
    }

    public boolean comesBefore(Address other) {
        return this.getZipCode() < other.getZipCode()? true:false;
    }
}
