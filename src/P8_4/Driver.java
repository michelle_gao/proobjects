package P8_4;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/18/13
 * Time: 9:12 PM
 * p8.4 implement a class Address.
 */
public class Driver {
    public static void main(String[] args) {
        Address aApollo = new Address("5258", "S Drexel Ave", "1G", "Chicago", "IL", 60615);
        Address Rocky = new Address("303", "E Wacker Drive", "2800", "Chicago", "IL", 60601);
        if (aApollo.comesBefore(Rocky)) {
            aApollo.print();
            Rocky.print();
        }
        else {
        Rocky.print();
        aApollo.print();
        }

    }
}
