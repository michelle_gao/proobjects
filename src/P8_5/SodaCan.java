package P8_5;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/18/13
 * Time: 9:43 PM
 * P8.5 Implement a class SodaCan with methods getSurfaceArea() and getVolume)
 */
public class SodaCan {
    private  double dHeight;
    private  double dRadius;

    public SodaCan(double dHeight, double dRadius) {
        this.dHeight = dHeight;
        this.dRadius = dRadius;
    }
    public double getSurfaceArea() {
        return this.dHeight * 2 * Math.PI * this.dRadius;
    }
    public double getVolume(){
        return  this.dHeight * Math.PI * this.dRadius * this.dRadius;
    }

}
