package P8_5;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/18/13
 * Time: 9:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class Driver {
    public static void main(String[] args) {
        SodaCan soCan= new SodaCan(5.5, 1.5);
        System.out.println("The surface area of the can is: " + soCan.getSurfaceArea());
        System.out.println("The Volume of the can is " + soCan.getVolume());
    }
}
