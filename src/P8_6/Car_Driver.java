package P8_6;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/18/13
 * Time: 10:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class Car_Driver {
    public static void main(String[] args) {
        Car myHybrid = new Car(50);
        myHybrid.addGas(20);
        myHybrid.drive(100);
        System.out.println("My Hybrid Car's Current Gas level is: " + myHybrid.getGasLevel());
    }
}
