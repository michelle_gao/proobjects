package P8_6;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/18/13
 * Time: 9:54 PM
 * P8.6 Implement a class car with properties.
 */
public class Car {
    private  double dFuelEfficiency;
    private  double dAmountOfFuel;

    public Car(double dFuelEfficiency) {
        this.dFuelEfficiency = dFuelEfficiency;
        this.dAmountOfFuel = 0;
    }

    public void drive(double dDistance) {
        double dGasReducing = dDistance/this.dFuelEfficiency;
        this.dAmountOfFuel -= dGasReducing;
    }

    public double getGasLevel() {
        return this.dAmountOfFuel;
    }

    public double addGas(double dGas) {
        this.dAmountOfFuel += dGas;
        return this.dAmountOfFuel;
    }
}
