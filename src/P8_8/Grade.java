package P8_8;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 8:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class Grade {
    private double dGradeNumeric;

    public Grade(String strGrade) {
        this.dGradeNumeric = this.translateGrade(strGrade);
    }
    public double getGradeNumeric() {
        return dGradeNumeric;
    }
    public double translateGrade(String strGrade) {
        switch (strGrade) {
            case "A": dGradeNumeric=4; break;
            case "A-": dGradeNumeric=3.70; break;
            case "B+": dGradeNumeric=3.33; break;
            case "B": dGradeNumeric=3; break;
            case "B-": dGradeNumeric=2.7; break;
            case "C+": dGradeNumeric=2.3; break;
            case "C": dGradeNumeric=2; break;
            case "C-": dGradeNumeric=1.7; break;
            case "D+": dGradeNumeric=1.3; break;
            case "D": dGradeNumeric=1; break;
            case "D-": dGradeNumeric=0.7; break;
            case "F":
            default:
                dGradeNumeric=0; break;
        }
        return dGradeNumeric;
    }
}
