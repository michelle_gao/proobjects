package P8_8;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/18/13
 * Time: 10:30 PM
 * P8.8 Implement  a class student.
 */
public class Student {
    private String strName;
    private double dGPA;
    private ArrayList<Grade> mGrades;


    public Student(String strName, ArrayList<Grade> mGrades) {
        this.strName = strName;
        this.mGrades = mGrades;
    }

    public String getName() {
        return this.strName;
    }

    public double getGpa () {
        double totalGpa = 0;
        int numberOfCourses=0;
        Iterator itr=mGrades.iterator();
        while (itr.hasNext()) {
            Object aGrade = itr.next();
            totalGpa += ((Grade)aGrade).getGradeNumeric();
            numberOfCourses++;
        }
        return totalGpa/numberOfCourses;

    }

    public void addGrade(Grade grd){

        mGrades.add(grd);
    }
}