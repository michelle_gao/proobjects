package P8_8;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class GradeDriver {
    public static void main(String[] args) {
        Grade grade1 = new Grade("A");
        Grade grade2 = new Grade("A-");
        Grade grade3 = new Grade("A");
        Grade grade4 = new Grade("B+");
        Grade grade5 = new Grade("B");
        Grade grade6 = new Grade("B+");
        Grade grade7 = new Grade("A");
        Grade grade8 = new Grade("A");
        Grade grade9 = new Grade("A-");
        Grade grade10 = new Grade("A-");
        ArrayList<Grade> mGrade = new ArrayList<Grade>();
        Student studentM = new Student("Michelle", mGrade);
        studentM.addGrade(grade1);
        studentM.addGrade(grade2);
        studentM.addGrade(grade3);
        studentM.addGrade(grade4);
        studentM.addGrade(grade5);
        studentM.addGrade(grade6);
        studentM.addGrade(grade7);
        studentM.addGrade(grade8);
        studentM.addGrade(grade9);
        studentM.addGrade(grade10);
        double dGpa = studentM.getGpa();
        System.out.printf(studentM.getName() + "'s GPA is: %.2f",dGpa);
    }
}
