package P8_11;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 2:08 PM
 * P8.11 Provide a class for authoring a simple letter.
 */
public class Letter {
    private String from;
    private String to;
    private String body;

    public Letter(String from, String to) {
        this.from = from;
        this.to = to;
        body = "";

    }

    public void addLine(String Line) {
        body +=Line;
        body += "\n";
    }
    public String getText() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("Dear " + from + ":" + "\n\n");
        strBuilder.append(body + "\n");
        strBuilder.append("Sincerely," + "\n\n");
        strBuilder.append(to);
        return strBuilder.toString();
    }
}
