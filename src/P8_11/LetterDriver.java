package P8_11;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/19/13
 * Time: 2:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class LetterDriver {
    public static void main(String[] args) {
        Letter mLetter = new Letter("Michelle Gao", "Jennifer Jiao");
        System.out.print("Please enter the first line for the letter: ");
        Scanner sc = new Scanner(System.in);
        mLetter.addLine(sc.nextLine());
        System.out.print("Please enter the second line for the letter: ");
        mLetter.addLine(sc.nextLine());
        System.out.println(mLetter.getText());
    }
}
